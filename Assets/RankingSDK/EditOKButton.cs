﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;


public class EditOKButton : MonoBehaviour {

    public Text nameText;
    public GameObject NameEditCanvas;
    public Text Myrank;
    public GameObject ErrPanel;

    public GameObject NGwords;

    private NGList ngList;



    void Start()
    {
        ngList = NGwords.GetComponent<NGList>();
    }


    public void GetName()
    {
        string strAfter = "";

        //入力されているか？
        if (nameText.text == string.Empty)
            return;

        if (!IsKana(nameText.text))
        {
            ErrPanel.active = true;
            return;
        }

        if(NGCheck(nameText.text))
        {
            ErrPanel.active = true;
            return;
        }


        if (nameText.text.Length > 5) strAfter = nameText.text.Remove(5);

        else strAfter = nameText.text;

        //ユーザー名登録
        RankingManager.Instance.SignUp(strAfter);

        NameEditCanvas.active = false;
    }

    

    public static bool IsKana(string target)
    {
        foreach (var chara in target)
        {
            var charaData = Encoding.UTF8.GetBytes(chara.ToString());

            if (charaData.Length != 3)
            {
                return false;
            }

            var charaInt = (charaData[0] << 16) + (charaData[1] << 8) + charaData[2];

            if (charaInt < 0xe38181 || charaInt > 0xe38293)
            {
                return false;
            }
        }

        return true;
    }

    private bool NGCheck(string name)
    {
        


        for (int i = 0; i < ngList.ngword.Length; i++ )
        {
            if ( 0 <= name.IndexOf(ngList.ngword[i]))
            {
                return true;
            }
        }

            return false;
    }

}
