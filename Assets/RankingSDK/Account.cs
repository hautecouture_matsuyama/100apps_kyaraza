﻿using UnityEngine;
using System.Collections;

public class Account : MonoBehaviour {

    public GameObject NameEditCanvas;

    private int cheak;

    void Awake()
    {
        QualitySettings.vSyncCount = 0; // VSyncをOFFにする
        Application.targetFrameRate = 60; // ターゲットフレームレートを60に設定

        //Editorか
        if (Debug.isDebugBuild)
            return;
        //ネット接続されているか
        if (Application.internetReachability == NetworkReachability.NotReachable)
            return;
        //アカウントは存在するか
        if (RankingManager.Instance.CheckAccount())
            return;
		
		StartCoroutine(ShowNameEdit());
        
                
    }

    private IEnumerator ShowNameEdit()
    {
        yield return new WaitForSeconds(0.5f);

        NameEditCanvas.SetActive(true);

    }

}
